
var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "domain": [-5, 15], "range": [-5, 5]});

function f(i){
    return 3*(Math.sin(2*i)/5*Math.sin(i) - i/30)+Math.random();;
}

var init = [];
var dom = [];
for (var i=0; i<=3*Math.PI; i += 0.3){
    init.push(f(i));
    dom.push(i);
}

//plt.plot(dom, init);

var grid_red = [];

for (var i in init){
    var x = dom[i];
    var y = init[i] + Math.random()/4;
    for (var j=0; j<=1; j+= 0.1){
        grid_red.push([x, y+j]);
    }
}

//
//var i, j;
//
//for (j=-Math.PI; j<=Math.PI; j+=0.1){
//    for (i=-0; i<=10; i+=1){
//        data.push([i, Math.sin(10*j)]);
//    }
//}
//
plt.cvector_field(grid_red, function(d){return [d[0], 3*Math.sin(3*(d[1]+Math.PI))];}, {"head-size": 3, "length": 2, "linewidth":.5, "color": "red", "linecolor": "red", "legend": "Campo|$ (-y, x) $|"});
//plt.opaque(true);




var grid_blue = [];

for (var i in init){
    var x = dom[i];
    var y = init[i];
    for (var j=0; j>=-1; j-= 0.1){
        grid_blue.push([x, y+j]);
    }
}

//
//var i, j;
//
//for (j=-Math.PI; j<=Math.PI; j+=0.1){
//    for (i=-0; i<=10; i+=1){
//        data.push([i, Math.sin(10*j)]);
//    }
//}
//
plt.cvector_field(grid_blue, function(d){return [d[0], 3*Math.sin(3*(d[1]+Math.PI))];}, {"head-size": 3, "length": 2, "linewidth":.5, "color": "blue", "linecolor": "blue", "legend": "Campo|$ (-y, x) $|"});
//plt.opaque(true);

