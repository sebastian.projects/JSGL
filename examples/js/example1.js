
var plt = new graph_function({"height": 250,"width": 350, "container": "#here1", "domain": [-1, 1], "range": [-1, 1]});
var plt2 = new graph_function({"height": 250,"width": 350, "container": "#here2", "domain": [-1, 1], "range": [-1, 1]});

var data = [];
var func = [];
var i, j;

for (i=-1; i<= 1; i+=.1){
    for (j=-1; j<= 1; j+=.1){
        data.push([i, j]);
        func.push([Math.sin(j), Math.sin(i)]);
    }
}

function new_square(){
    plt.set_new_square([-5*(Math.abs(Math.sin(counter))+1/5), 5*(Math.abs(Math.sin(counter))+1/5)], [-1, 1]);
    plt2.set_new_square([-5*(Math.abs(Math.sin(counter))+1/5), 5*(Math.abs(Math.sin(counter))+1/5)], [-1, 1]);
    counter += .02;
}

d3.select("#btn3").on("click", new_square);
plt.legend({"width": 140, "position": LEGEND_LEFT_UP, "container": "#leyenda1"})
plt2.legend({"width": 140, "position": LEGEND_LEFT_UP, "container": "#leyenda2"})

var counter=0;
window.setInterval(new_square, 100);

plt.dvector_field(data,func, {"conformal": false, "head-size": 2, "length": 20, "linewidth": .5, "color": "green", "linecolor": "green", "legend": "|$ (\\sin(y), \\sin(x)) $|"});
plt2.dvector_field(data,func, {"conformal": true, "head-size": 2, "length": 20, "linewidth": .5, "color": "green", "linecolor": "green", "legend": "|$ (\\sin(y), \\sin(x)) $|"});

plt.desactive_zoom_and_translation();
plt2.desactive_zoom_and_translation();