
//TODO: Aun no esta listo esto. Si se cambia de tamaño alguno de los dos y no el
//otro, el zoom no se hace bien para los scatters, ya que en este momento,
//esta utilizando una variable global para detectar el evento zoom.
var plt = new graph_function({"width": 200, "height": 300, "container": "#here1"});
var plt2 = new graph_function({"width": 200, "height": 300, "container": "#here2"});

var xdata = [];
var ydata = [];

var i;

for (i=-314.15; i<=314.15; i+=1){
    xdata.push(i);
}

for (i in xdata){
    ydata.push(100*Math.sin(xdata[i]/100));
}

function add_plot(){
plt.poligon([[[-10, -15], [-10, 15], [5, 0]]], {"color": "red", "center": [0, 0], "zoom": true});
    plt.plot(xdata, ydata, {"alpha": .5});
}
d3.select("#add").on("click", add_plot);

plt.scatter([100], [100], 14, {"color": "green"});
plt.scatter([-100, 0], [-100, 0], 30, {"color": "red"});
var group = plt2.poligon([[[-70, 0], [-80, -40], [4,6]], [[-70, -15], [-70, 15], [-40, 0]], [[70, -15], [70, 15], [40, 0]]], {"color": "red", "center": [0, 0], "rotate": 30, "zoom": true});
plt.symbol([0], [0], {"symbol": "triangle-up", "rotate": 15})
plt2.rectangle([-100, 0], [-100, 0], 30, 20, {"color": "blue", "rotate": 30, "zoom": true});
cosa = {3:"__default_fig ", 2:"__default_line "}
plt.poligon([[[130, -12], [130, 12], [152, 0]], [[0, 0], [130, 0]]], {"linecolor": "black", "color": "red", "center": [-200, -50], "_class": function(d){ return cosa[d.length];}, "rotate": -45});

plt.arrow(-200, 50, 45, 130, {"head-size": 2})
plt.arrow2(10, 10, -10, -10)
var i, j;
for (i=-10; i<= 10; i++){
    for (j=-10; j<= 10; j++){
            plt.arrow2(i*10, j*10, Math.sin(i/100), Math.cos(j/100));
    }
}