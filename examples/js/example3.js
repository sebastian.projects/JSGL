
var plt = new graph_function({"height": 500,"width": 700, "container": "#heré 1", "domain": [-350, 350], "range": [-110, 110]});
//plt.legend({"height": 3.0, "width": 50.0});

var xdata = [];
var ydata = [];
var zdata = [];

var i;

for (i=-314.15; i<=314.15; i+=1){
    xdata.push(i);
}

for (i in xdata){
    ydata.push(100*Math.sin(xdata[i]/100));
}

for (i in xdata){
    zdata.push(10*Math.log(Math.abs(xdata[i]/100)));
}

plt.plot(xdata, ydata, {"alpha": .5, "legend": "|$ 100\\sin(\\frac{x}{100}) $|", "color": "red"});
plt.plot(xdata, zdata, {"alpha": .5, "legend": "|$ 10\\log(|\\frac{x}{100}|) $|", "font-size": 5});
//plt.plot(xdata, ydata, {"alpha": .5, "legend": "a", "color": "red", "font-size": "9px"});
//plt.plot(xdata, zdata, {"alpha": .5, "legend": "b", "font-size": "9px"});