
var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "domain": [-350, 350], "range": [-110, 110], 'x': {'tick':10}});

var xdata = [];
var ydata = [];
var zdata = [[], [], [], []];

var i;

for (i=-314.15; i<=314.15; i+=100){
    xdata.push(i);
}

for (i in xdata){
    zdata[0].push(7*Math.log(Math.abs(xdata[i]/100))+60);
}

for (i in xdata){
    zdata[1].push(7*Math.log(Math.abs(xdata[i]/100))+20);
}

for (i in xdata){
    zdata[2].push(7*Math.log(Math.abs(xdata[i]/100))-20);
}

for (i in xdata){
    zdata[3].push(7*Math.log(Math.abs(xdata[i]/100))-60);
}

plt.plot(xdata, zdata[0], {"alpha": .5, "color": "steel-blue", "legend": "Interpolacion linear", "interpolate": "linear"});
plt.plot(xdata, zdata[1], {"alpha": .5, "color": "red", "legend": "Interpolacion step-before", "interpolate": "step-before"});
plt.plot(xdata, zdata[2], {"alpha": .5, "color": "green", "legend": "Interpolacion step-after", "interpolate": "step-after"});
plt.plot(xdata, zdata[3], {"alpha": .5, "color": "purple", "legend": "Interpolacion basis", "interpolate": "basis"});
var group = plt.poligon([[[-50, -90], [50, -70], [50, -100], [-50, -100]]], {"color": "red", "zoom": true, "legend": "Y con poligonos", "interpolate": "basis"});