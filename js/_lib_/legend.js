
graph_function.prototype.legend = function(args){
    var position;
    var place;
    var width;
    var container_txt;
    var container;

    if (typeof args == "undefined"){
        args = {};
    }

    if ("container" in args){
        container_txt = args["container"];
        container = d3.select(container_txt);
    } else {
        container_txt = "self";
        container = d3.select(this.__container);
    }
    container.style("position", "relative");

    if ("position" in args){
        position = args["position"];
        if (container_txt == "self"){
            if (position in this.__LEGEND_PLACES){
                    position = this.__LEGEND_PLACES[position];
            }
        }
    } else {
        position = this.__LEGEND_PLACES[LEGEND_RIGHT_UP];
    }

    if ("width" in args){
        width = args["width"];
    } else {
        width = this.__LEGEND_WIDTH;
    }
    this.__LEGEND_WIDTH = width;

    //Esta altura es la de cada item.
    if ("height" in args){
        this.__LEGEND_ITEM_HEIGHT = args["height"];
    }

    this.legend_container = container.append("div").attr("class", "__legend_class")
                                                   .attr("align", "left")
                                                   .style("left", position[0] + "px")
                                                   .style("top", position[1] + "px")
                                                   .style("width", width + "px")
                                                   .style("height", 0 + "px");

}

graph_function.prototype.__draw_symbol = function(container, args){
    var opacity;
    var symbol;
    var draw;
    var size;
    var color;

    if ("color" in args){
        color = args["color"];
    }

    if ("symbol" in args){
        symbol = args["symbol"];
    } else {
        symbol = "curve";
    }

    if ("opacity" in args){
        opacity = args["opacity"];
    }

    if ("lsize" in args){
        size = args["size"];
    } else {
        size = 0.3;
    }

    switch (symbol){
        case "curve":
            draw = container.append("svg").attr("width", 15)
                                          .attr("height", this.__LEGEND_ITEM_HEIGHT)
                                          .selectAll("symbols")
                                              .data([0, 0])
                                              .enter().append("path")
                                              .attr("d", this.CLASS_BASIC_LINE_PIXEL([[3, this.__LEGEND_ITEM_HEIGHT/2], [20, this.__LEGEND_ITEM_HEIGHT/2]]))
                                              .attr("class", "__default_line")
                                              .style("stroke-width", 5*size);
            if (typeof color !== "undefined"){
                draw.style("stroke", color);
            }
            break;

        default :
            draw = container.append("svg").attr("width", 15)
                                          .attr("height", this.__LEGEND_ITEM_HEIGHT)
                                          .selectAll("symbols")
                                              .data([0, 0])
                                              .enter().append("path")
                                              .attr("class", "__default_fig")
                                              .attr("d", d3.svg.symbol().type( symbol )
                                                                        .size( 100*size ) ([0, 0]))
                                              .attr("transform", __translate(12, this.__LEGEND_ITEM_HEIGHT/2));
            if (typeof color !== "undefined"){
                draw.style("fill", color);
            }
            break;

    }

    if (typeof opacity !== "undefined"){
        draw.style("opacity", opacity);
    }
}

graph_function.prototype.add_to_legend = function(args){
    var text;
    var id = "ID" + Math.floor(100000*Math.random());

    if (typeof args == "undefined"){
        args = {};
    }

    if (typeof this.legend_container == "undefined"){
        this.legend();
    }

    this.__LEGEND_HEIGHT += this.__LEGEND_ITEM_HEIGHT+17;

    this.legend_container.style("height", this.__LEGEND_HEIGHT+"px")
                         .style("width", this.__LEGEND_WIDTH-10+"px");
    var div = this.legend_container.append("div").attr("class", "__legend_item")
                       .style("height", (this.__LEGEND_ITEM_HEIGHT+17) + "px");

    this.__draw_symbol(div, args);

    if ("legend" in args){
        text = args["legend"];
    } else {
        text = "";
    }

    div.append("label").style("width", (this.__LEGEND_WIDTH-40) + "px")
                       .style("height", (this.__LEGEND_ITEM_HEIGHT+7) + "px")
                       .style("left", 20+"px")
                       .attr("id", id);
//                       .text();
    __render_tex(text, "#" + id, args);
}